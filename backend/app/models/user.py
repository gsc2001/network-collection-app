from datetime import datetime

from typing import List, Dict, Union, Optional
from .base import Base, ObjectID, QuestionnaireEnum

from bson.objectid import ObjectId


class StateInResponse(Base):
    """Each questionnaire state"""
    state: Dict[str, Union[ObjectID, Dict[str, List[str]]]] = dict()


class StatesInResponse(Base):
    """The last saved state for a given user"""
    states: List[StateInResponse] = list()


class Audit(Base):
    """Audit log entry"""

    state: Dict[str, ObjectID]
    questionnaire_id: str
    username: str
    timestamp: datetime = datetime.utcnow()
