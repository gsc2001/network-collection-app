from enum import Enum
from datetime import datetime, timezone

from pydantic import BaseConfig, BaseModel
from bson.objectid import ObjectId


class QuestionnaireEnum(str, Enum):
    hums = 'hums'
    k10 = 'k10'
    mspss = 'mspss'
    ssq6 = 'ssq6'
    idip20 = 'idip20'


class ObjectID(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(str(v)):
            return ValueError(f"Not a valid ObjectId: {v}")
        return ObjectId(str(v))


class Base(BaseModel):
    class Config(BaseConfig):
        allow_population_by_alias = True
        arbitrary_types_allowed = True
        json_encoders = {
            datetime: lambda dt: dt.replace(tzinfo=timezone.utc)
            .isoformat()
            .replace("+00:00", "Z"),
            ObjectId: ObjectID
        }
