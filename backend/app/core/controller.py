import hashlib
import urllib
import logging
import random
from typing import Union

from cas import CASClient
from fastapi import APIRouter, Depends, HTTPException
import jwt
import ldap
from motor.motor_asyncio import AsyncIOMotorClient
from starlette.responses import RedirectResponse
import ujson

from app.utils.cas import get_cas
from app.utils.token import verify_token
from app.utils.mongodb import get_database
from app.utils.ldap import get_ldap, AsyncLDAPClient
from app.models.base import QuestionnaireEnum
from app.models.user import StateInResponse, Audit
from app.models.questionnaire import QuestionnaireInResponse
from config import SECRET_KEY

router = APIRouter()


@router.get("/login", tags=["auth"])
async def login_route(next: str = "/", ticket: str = None, cas_client: CASClient = Depends(get_cas),
                      db: AsyncIOMotorClient = Depends(get_database)):
    """login using CAS login

    """
    if not ticket:
        # No ticket, the request come from end user, send to CAS login
        cas_login_url = cas_client.get_login_url()
        return RedirectResponse(url=cas_login_url)

    _user, attributes, _ = cas_client.verify_ticket(ticket)
    if not _user:
        return {
            "success": 0,
            "message": "Invalid user! Retry logging in!"
        }
    else:
        username = hashlib.sha256(
            f"{attributes['RollNo']}_{_user}".encode()).hexdigest()
        logging.debug(
            f"CAS verify ticket response: [username] {_user} <-> {username}")

        existing = await db["core"]["users"].find_one({"username": username})
        if existing:
            # TODO add provision to add mapping and logging info (in case of indirect user addition)
            await db["core"]["users"].update_one({
                "username": username}, {"$set": {"last_login": attributes["authenticationDate"]}})
        else:
            # add the initial state as unanswered
            mappings = {}
            async for questionnaire in db["core"]["questions"].find():
                if questionnaire['_id'] == "ssq6":
                    # create different mapping for inter questionnaire
                    mapping = {
                        str(question["_id"]): {
                            str(option["_id"]): [] for option in questionnaire['options']
                        } for question in questionnaire['questions']
                    }
                else:
                    default = [option["_id"] for option in questionnaire["options"]
                               if option["label"] == "Unattempted"][0]
                    mapping = {
                        str(question["_id"]): default for question in questionnaire["questions"]
                    }
                mappings.update({
                    str(questionnaire["_id"]): mapping
                })

            _res = await db["core"]["users"].insert_one({
                "username": username,
                "last_login": attributes["authenticationDate"],
                "first_login": attributes["authenticationDate"],
                "state": mappings
            })
        jwt_token = jwt.encode({'username': username},
                               str(SECRET_KEY), algorithm="HS256").decode()
        user_response = urllib.parse.urlencode({
            "success": 1,
            "data": urllib.parse.urlencode({
                "username": username,
                "token": jwt_token
            })
        })
        redirect_url = f"{next}#/?user={user_response}"
        return RedirectResponse(url=redirect_url)


@router.get("/questions/{questions_id}", response_model=QuestionnaireInResponse,
            dependencies=[Depends(verify_token)], tags=["questions"])
async def get_questionnaire(questions_id: QuestionnaireEnum,
                            db: AsyncIOMotorClient = Depends(get_database)) -> QuestionnaireInResponse:
    """
    Retrieve questionnaire given the `question_id`

    :param questions_id: QuestionnaireEnum -> ID of the questionnaire, eg: "hums"
    :param db: [AsyncIOMotorClient] -> async db connector
    :returns questionnaire: QuestionnaireInResponse -> relevant questionnaire

    """
    questionnaire = await db["core"]["questions"].find_one(
        {"_id": questions_id})
    if questionnaire:
        if questionnaire['random']:
            random.shuffle(questionnaire['questions'])
        return QuestionnaireInResponse(**questionnaire)
    else:
        raise HTTPException(
            status_code=404, detail=f"Questionnaire {questions_id} not found!")


@router.get("/state/{questions_id}", tags=["user", "questions", "state", "fetch"])
async def get_state(questions_id: QuestionnaireEnum, token: dict = Depends(verify_token),
                    db: AsyncIOMotorClient = Depends(get_database)):
    """
    Retrieve the last saved state of the user

    :param questions_id: QuestionnaireEnum -> ID of the questionnaire, eg: "hums"
    :param db: [AsyncIOMotorClient] -> async db connector
    :returns state: dict[str, int] -> state for the given questionnaire
    """
    _user = await db["core"]["users"].find_one(token)
    try:
        questionnaire_state = _user["state"].get(questions_id, {})
        return {
            "state": questionnaire_state
        } if questions_id == QuestionnaireEnum.ssq6 else StateInResponse(state=questionnaire_state)
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"Questionnaire {questions_id} state not loadable for user! | {e}")


@router.post("/state/{questions_id}", tags=["user", "questions", "state", "update"])
async def set_state(questions_id: QuestionnaireEnum, new_state: StateInResponse, token: dict = Depends(verify_token),
                    db: AsyncIOMotorClient = Depends(get_database)):
    """
    Set the latest state and push update to audit log

    :param questions_id: QuestionnaireEnum -> ID of questionnaire
    :param db: [AsyncIOMotorClient] -> async db connector
    :returns has_saved: dict -> success/failure
    """
    try:
        _res = await db["core"]["users"].update_one(token, {"$set": {
            f"state.{questions_id}": new_state.state
        }})
        audit_entry = Audit(
            username=token['username'], state=new_state.state, questionnaire_id=questions_id)
        _audit_res = await db["core"]["audit"].insert_one(audit_entry.dict())
        # TODO add background task for metric calculation
        return {"success": True}
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Progress not saved! {e}")


@router.get("/search/{year}/{branch}", dependencies=[Depends(verify_token)], tags=["user", "search"])
async def get_search_query(year: str, branch: str, name: str = None, ldap_client: AsyncLDAPClient = Depends(get_ldap), db: AsyncIOMotorClient = Depends(get_database)):
    """
    Retrieve search results by performing dynamic async ldap search

    :param query: str -> search query from user
    :param ldap: [AsyncLDAPClient]: async ldap connector
    :param db: [AsyncIOMotorClient]: async db connector
    :returns search_result: dict -> search query result
    """
    try:
        base = ",ou=Users,dc=iiit,dc=ac,dc=in"
        if branch.endswith("d"):
            base = f"ou={branch},ou={year}dual,ou=ug,ou=Research{base}"
        else:
            base = f"ou={branch},ou={year},ou=ug,ou=Students{base}"

        querystring = f"uid={name},{base}" if name else base
        search_id = ldap_client.search(
            querystring, ldap.SCOPE_SUBTREE, '(objectClass=*)')
        results = ldap_client.allresults(search_id)
        data = [
            res_data[0][1] for _, res_data, _, _ in results if all(
                [k in res_data[0][1] for k in ['sambaSID', 'mail', 'cn']])
        ]
        data = [
            {
                k: v[0] if isinstance(v, list) and len(v) == 1 else v for k, v in record.items()
            } for record in data
        ]
        query_result = [
            {
                "batch": year,
                "branch": branch,
                "id": hashlib.sha256(f"{_record['sambaSID'].decode()}_{_record['mail'].decode()}".encode()).hexdigest(),
                "name": _record["cn"],
                "gender": _record.get("gender", None)
            } for _record in data
        ]

        return {
            "success": True,
            "data": query_result
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Search failed! {e}")
