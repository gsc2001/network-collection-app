import ldap
import ldap.resiter


class AsyncLDAPClient(ldap.ldapobject.LDAPObject, ldap.resiter.ResultProcessor):
    # to make processing async
    pass


class LDAP:
    client: AsyncLDAPClient = AsyncLDAPClient("ldap://ldap.iiit.ac.in")


ldap_client = LDAP()


async def get_ldap() -> AsyncLDAPClient:
    return ldap_client.client
